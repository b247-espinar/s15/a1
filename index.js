console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

	let fnameKo = "Avie Joy";
	let	lnameKo = "Espinar";
	const ageKo = 27;
	let hobbiesKo = ["gumala ", " ng gumala", " at gumala"];
	let addKo = {
		houseNumber: 634,
		street: 'Gaanan St',
		city: 'Valenzuela',
		state: 'Metro Manila'
	}

	let firstNameKo = "First Name: " + fnameKo;
	let lastNameKo = "Last Name: " + lnameKo;
	let myageKo = "Age: " + ageKo;
	let myhobbiesKo = "Hobbies: ";
	let addressKo = "Home Address: \n";
	console.log(firstNameKo + '\n' + lastNameKo + '\n' + myageKo + '\n');
	console.log(myhobbiesKo);
	console.log(hobbiesKo);
	console.log(addressKo);
	console.log(addKo);


/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	let name = "My full name is: " + fullName;
	console.log(name);

	let age = 40;
	let currentAge = "My current age is: " + age;
	console.log(currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	let friendNames = "My Friends are: ";
	console.log(friendNames);
	console.log(friends);

	let myFull = "My Full Profile: ";
	console.log(myFull);


	let profile = {

		username: 'captain_america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false
	}

	console.log(profile);


	let fullNamee = "Bucky Barnes";
	let fname = "My bestfriend is: ";
	console.log(fname + fullNamee);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	let loc = "I was found frozen in: ";
	console.log(loc + lastLocation);


	